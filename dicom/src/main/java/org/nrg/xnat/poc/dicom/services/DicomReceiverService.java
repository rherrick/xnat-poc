package org.nrg.xnat.poc.dicom.services;

import org.nrg.xnat.poc.dicom.entities.ApplicationEntity;

import java.util.List;

public interface DicomReceiverService {
    List<ApplicationEntity> findAll();

    ApplicationEntity findOne(final long id);

    Long create(final ApplicationEntity resource);

    ApplicationEntity getById(final long id);

    void update(final ApplicationEntity resource);

    void deleteById(final long id);
}
