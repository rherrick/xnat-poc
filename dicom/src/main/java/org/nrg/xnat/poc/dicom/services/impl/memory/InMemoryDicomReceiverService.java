package org.nrg.xnat.poc.dicom.services.impl.memory;

import org.nrg.xnat.poc.dicom.entities.ApplicationEntity;
import org.nrg.xnat.poc.dicom.services.DicomReceiverService;
import org.nrg.xnat.poc.dicom.generated.GeneratedEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InMemoryDicomReceiverService implements DicomReceiverService {
    public InMemoryDicomReceiverService() {
        final GeneratedEntity entity = new GeneratedEntity();
        System.out.println(entity.getEntityMessage());
        create(new ApplicationEntity("XNAT", 8104, "central.xnat.org", "XNAT Central primary C-STORE endpoint"));
        create(new ApplicationEntity("CNDA", 8104, "cnda.wustl.edu", "CNDA primary C-STORE endpoint"));
        create(new ApplicationEntity("CCIR", 8104, "cnda.wustl.edu", "CCIR C-STORE endpoint"));
    }

    @Override
    public List<ApplicationEntity> findAll() {
        return new ArrayList<>(_entities.values());
    }

    @Override
    public ApplicationEntity findOne(final long id) {
        return _entities.get(id);
    }

    @Override
    public Long create(final ApplicationEntity resource) {
        final long id = _currentIndex++;
        resource.setId(id);
        _entities.put(id, resource);
        return id;
    }

    @Override
    public ApplicationEntity getById(final long id) {
        return _entities.get(id);
    }

    @Override
    public void update(final ApplicationEntity resource) {
        _entities.put(resource.getId(), resource);
    }

    @Override
    public void deleteById(final long id) {
        _entities.remove(id);
    }

    private long _currentIndex = 0L;
    private final Map<Long, ApplicationEntity> _entities = new HashMap<>();
}
