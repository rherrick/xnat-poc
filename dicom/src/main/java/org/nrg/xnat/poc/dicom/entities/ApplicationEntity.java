package org.nrg.xnat.poc.dicom.entities;

public class ApplicationEntity {
    public ApplicationEntity() {

    }

    public ApplicationEntity(final String title, final int port, final String host) {
        this(-1L, title, port, host, "");
    }

    public ApplicationEntity(final String title, final int port, final String host, final String description) {
        this(-1L, title, port, host, description);
    }

    public ApplicationEntity(final long id, final String title, final int port, final String host) {
        this(id, title, port, host, "");
    }

    public ApplicationEntity(final long id, final String title, final int port, final String host, final String description) {
        _id = id;
        setTitle(title);
        setPort(port);
        setHost(host);
        setDescription(description);
    }

    public long getId() {
        return _id;
    }

    public void setId(final long id) {
        _id = id;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(final String title) {
        _title = title;
    }

    public int getPort() {
        return _port;
    }

    public void setPort(final int port) {
        _port = port;
    }

    public String getHost() {
        return _host;
    }

    public void setHost(final String host) {
        _host = host;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(final String description) {
        _description = description;
    }

    private long _id;
    private String _title;
    private int _port;
    private String _host;
    private String _description;
}

