package org.nrg.xnat.poc.web.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("/")
    public String showHomePage(final Model model) {
        model.addAttribute("name", "Franz Ferdinand");
        return "index";
    }
}
