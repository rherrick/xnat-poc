package org.nrg.xnat.poc.web.config;

import org.nrg.framework.processors.XnatModuleBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PocWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    public void onStartup(ServletContext context) throws ServletException {
        super.onStartup(context);
        _log.error("context.getContextPath(): " + context.getContextPath());
        _log.error("context.getServletContextName(): " + context.getServletContextName());
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        final List<Class<?>> configClasses = new ArrayList<>();
        configClasses.add(RootConfig.class);
        configClasses.addAll(getModuleConfigs());
        return configClasses.toArray(new Class[configClasses.size()]);
    }

    private List<Class<?>> getModuleConfigs() {
        final List<Class<?>> moduleConfigs = new ArrayList<>();
        try {
            final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            final Resource[] resources = resolver.getResources("classpath*:META-INF/xnat/**/*-module.properties");
            for (final Resource resource : resources) {
                final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
                final XnatModuleBean module = new XnatModuleBean(properties);
                final Class<?> moduleConfig = module.getConfigClass();
                moduleConfigs.add(moduleConfig);
            }
        } catch (IOException e) {
            throw new RuntimeException("An error occurred trying to locate XNAT module definitions.");
        } catch (ClassNotFoundException e) {
            _log.error("Did not find a class specified in a module definition.", e);
        }

        return moduleConfigs;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[0];
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    private static final Logger _log = LoggerFactory.getLogger(PocWebApplicationInitializer.class);
}
