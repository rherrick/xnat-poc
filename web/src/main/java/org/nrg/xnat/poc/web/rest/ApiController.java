package org.nrg.xnat.poc.web.rest;

import com.google.common.base.Preconditions;
import io.swagger.annotations.*;
import org.nrg.xnat.poc.dicom.entities.ApplicationEntity;
import org.nrg.xnat.poc.dicom.services.DicomReceiverService;
import org.nrg.xnat.poc.web.tools.RestPreconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "The XNAT POC DICOM API")
@RestController
@RequestMapping(value = "/api/dicom")
class ApiController {

    @ApiOperation(value = "Get list of available DICOM receivers.", notes = "This function returns a list of all available DICOM receivers on the XNAT system.", response = ApplicationEntity.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "An array of application entities"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<ApplicationEntity> findAll() {
        return service.findAll();
    }

    @ApiOperation(value = "Get the indicated DICOM receiver.", notes = "This function returns the requested DICOM receiver.", response = ApplicationEntity.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The requested application entity"),
            @ApiResponse(code = 404, message = "The requested application entity was not found"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ApplicationEntity findOne(@ApiParam("The ID of the application entity to be retrieved") @PathVariable("id") Long id) {
        return RestPreconditions.checkFound(service.findOne(id));
    }

    @ApiOperation(value = "Create the submitted DICOM receiver.", notes = "This function creates the submitted DICOM receiver and returns the ID of the newly created object.", response = ApplicationEntity.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The requested application entity"),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Long create(@RequestBody ApplicationEntity resource) {
        Preconditions.checkNotNull(resource);
        return service.create(resource);
    }

    @ApiOperation(value = "Update the submitted DICOM receiver.", notes = "This function updates the DICOM receiver with the indicated ID from the properties set on the application entity submitted in the request body.", response = ApplicationEntity.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The requested application entity"),
            @ApiResponse(code = 404, message = "The indicated application entity was not found."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@ApiParam("The ID of the application entity to be updated") @PathVariable("id") Long id, @RequestBody ApplicationEntity resource) {
        Preconditions.checkNotNull(resource);
        RestPreconditions.checkFound(service.getById(resource.getId()));
        service.update(resource);
    }

    @ApiOperation(value = "Delete the indicated DICOM receiver.", notes = "This function deletes the DICOM receiver with the indicated ID.", response = ApplicationEntity.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The indicated application entity was successfully deleted."),
            @ApiResponse(code = 404, message = "The indicated application entity was not found."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@ApiParam("The ID of the application entity to be deleted") @PathVariable("id") Long id) {
        service.deleteById(id);
    }

    @Autowired
    private DicomReceiverService service;
}