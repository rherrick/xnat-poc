package org.nrg.xnat.poc.web.config;

import org.nrg.framework.processors.XnatModuleRegistryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.nrg.xnat.poc.dicom.services.impl.memory")
public class ServicesConfig {
    @Bean
    public BeanDefinitionRegistryPostProcessor xnatModuleRegistryPostProcessor() {
        return new XnatModuleRegistryPostProcessor();
    }
}