package org.nrg.xnat.poc.web.tools;

import org.nrg.xnat.poc.web.exceptions.ResourceNotFoundException;

public class RestPreconditions {
    public static <T> T checkFound(final T resource) {
        if (resource == null) {
            throw new ResourceNotFoundException();
        }
        return resource;
    }
}