<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%--@elvariable id="name" type="java.lang.String"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="display.title" arguments="${name}"/></title>
</head>
<body>
<spring:message code="display.hello" arguments="${name}"/>
</body>
</html>
