package org.nrg.xnat.poc.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.xnat.poc.web.config.WebConfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class})
@WebAppConfiguration
public class SpringContextTest {
    @Test
    public void whenSpringContextIsInstantiated_thenNoExceptions() {
        // When
    }
}