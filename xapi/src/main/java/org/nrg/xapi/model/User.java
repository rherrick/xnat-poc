package org.nrg.xapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

@ApiModel(description = "Contains the properties that define a user on the system.")
public class User {
    public User() {

    }

    public User(final String username, final String firstname, final String lastname, final String email, final boolean isAdmin, final String password, final UserAuth authorization) {
        _username = username;
        _firstname = firstname;
        _lastname = lastname;
        _email = email;
        _isAdmin = isAdmin;
        _dbName = "";
        _password = password;
        _salt = "";
        _lastModified = new Date();
        _authorization = authorization;
    }

    /**
     * The user's unique key.
     **/
    @ApiModelProperty(value = "The user's unique key.")
    @JsonProperty("id")
    public Integer getId() {
        return _id;
    }

    public void setId(Integer id) {
        _id = id;
    }

    /**
     * The user's login name.
     **/
    @ApiModelProperty(value = "The user's login name.")
    @JsonProperty("username")
    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }

    /**
     * The user's first name.
     **/
    @ApiModelProperty(value = "The user's first name.")
    @JsonProperty("firstname")
    public String getFirstname() {
        return _firstname;
    }

    public void setFirstname(String firstname) {
        _firstname = firstname;
    }

    /**
     * The user's last name.
     **/
    @ApiModelProperty(value = "The user's last name.")
    @JsonProperty("lastname")
    public String getLastname() {
        return _lastname;
    }

    public void setLastname(String lastname) {
        _lastname = lastname;
    }

    /**
     * The user's _email address.
     **/
    @ApiModelProperty(value = "The user's email address.")
    @JsonProperty("email")
    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    /**
     * Whether the user is enabled.
     **/
    @ApiModelProperty(value = "Whether the user is enabled.")
    @JsonProperty("enabled")
    public boolean isEnabled() {
        return _isEnabled;
    }

    public void setEnabled(final boolean isEnabled) {
        _isEnabled = isEnabled;
    }

    /**
     * Whether the user is verified.
     **/
    @ApiModelProperty(value = "Whether the user is verified.")
    @JsonProperty("verified")
    public boolean isVerified() {
        return _isVerified;
    }

    public void setVerified(final boolean isVerified) {
        _isVerified = isVerified;
    }

    /**
     * Whether the user is a site administrator.
     **/
    @ApiModelProperty(value = "Whether the user is a site administrator.")
    @JsonProperty("admin")
    public boolean isAdmin() {
        return _isAdmin;
    }

    public void setAdmin(final boolean isAdmin) {
        _isAdmin = isAdmin;
    }

    /**
     * The user's primary database (deprecated).
     **/
    @ApiModelProperty(value = "The user's primary database (deprecated).")
    @JsonProperty("dbName")
    public String getDbName() {
        return _dbName;
    }

    public void setDbName(String dbName) {
        _dbName = dbName;
    }

    /**
     * The user's encrypted _password.
     **/
    @ApiModelProperty(value = "The user's encrypted password.")
    @JsonProperty("password")
    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }

    /**
     * The _salt used to encrypt the user's _password.
     **/
    @ApiModelProperty(value = "The salt used to encrypt the user's password.")
    @JsonProperty("salt")
    public String getSalt() {
        return _salt;
    }

    public void setSalt(String salt) {
        _salt = salt;
    }

    /**
     * The date and time the user record was last modified.
     **/
    @ApiModelProperty(value = "The date and time the user record was last modified.")
    @JsonProperty("lastModified")
    public Date getLastModified() {
        return _lastModified;
    }

    public void setLastModified(Date lastModified) {
        _lastModified = lastModified;
    }

    /**
     * The user's authorization record used when logging in.
     **/
    @ApiModelProperty(value = "The user's authorization record used when logging in.")
    @JsonProperty("authorization")
    public UserAuth getAuthorization() {
        return _authorization;
    }

    public void setAuthorization(UserAuth authorization) {
        _authorization = authorization;
    }

    @JsonIgnore
    public String getFullname() {
        return String.format("%s %s", getFirstname(), getLastname());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class User {\n");

        sb.append("  id: ").append(_id).append("\n");
        sb.append("  username: ").append(_username).append("\n");
        sb.append("  firstname: ").append(_firstname).append("\n");
        sb.append("  lastname: ").append(_lastname).append("\n");
        sb.append("  email: ").append(_email).append("\n");
        sb.append("  dbName: ").append(_dbName).append("\n");
        sb.append("  password: ").append(_password).append("\n");
        sb.append("  salt: ").append(_salt).append("\n");
        sb.append("  lastModified: ").append(_lastModified).append("\n");
        sb.append("  authorization: ").append(_authorization).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    private Integer _id = null;
    private String _username = null;
    private String _firstname = null;
    private String _lastname = null;
    private String _email = null;
    private boolean _isEnabled = true;
    private boolean _isVerified = true;
    private boolean _isAdmin = false;
    private String _dbName = null;
    private String _password = null;
    private String _salt = null;
    private Date _lastModified = null;
    private UserAuth _authorization = null;
}
