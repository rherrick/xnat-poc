package org.nrg.xapi.config;

import org.nrg.framework.annotations.XnatModule;
import org.nrg.xapi.services.TestService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@XnatModule(value = "xapi", name = "XNAT Extended API", description = "This is the much more extended API than the old tiny one.", config = UserApiConfig.class)
@EnableWebMvc
@ComponentScan({"org.nrg.xapi.services.impl.inmemory", "org.nrg.xapi.rest"})
public class UserApiConfig {
    @Bean
    public TestService testService() {
        return new TestService();
    }
}
