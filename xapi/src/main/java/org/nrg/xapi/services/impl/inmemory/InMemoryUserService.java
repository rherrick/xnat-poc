package org.nrg.xapi.services.impl.inmemory;

import org.apache.commons.lang3.NotImplementedException;
import org.nrg.xapi.model.User;
import org.nrg.xapi.model.UserAuth;
import org.nrg.xapi.services.UserService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class InMemoryUserService implements UserService {
    private final Map<Integer, User> _users = new HashMap<>();
    private final Map<String, User> _usersByUsername = new HashMap<>();

    public InMemoryUserService() {
        create(new User("user1", "User", "One", "one@xnat.org", true, "simple", new UserAuth()));
        create(new User("user2", "User", "Two", "two@xnat.org", false, "simple", new UserAuth()));
        create(new User("user3", "User", "Three", "three@xnat.org", false, "simple", new UserAuth()));
        create(new User("user4", "User", "Four", "four@xnat.org", false, "simple", new UserAuth()));
        create(new User("user5", "User", "Five", "five@xnat.org", false, "simple", new UserAuth()));
        create(new User("user6", "User", "Six", "six@xnat.org", false, "simple", new UserAuth()));
    }

    @Override
    public List<String> getAllUsers() {
        return new ArrayList<>(_usersByUsername.keySet());
    }

    @Override
    public User getUserByUsername(final String username) {
        return _usersByUsername.get(username);
    }

    @Override
    public List<User> getUsersByProperties(final Map<String, Object> properties) {
        throw new NotImplementedException("This method has not yet been implemented.");
    }

    @Override
    public synchronized User create(final User user) {
        if (_usersByUsername.containsKey(user.getUsername())) {
            throw new RuntimeException("A user with the indicated username already exists.");
        }
        int id = getNextKey();
        user.setId(id);
        _users.put(id, user);
        _usersByUsername.put(user.getUsername(), user);
        return user;
    }

    @Override
    public User retrieve(final int id) {
        return _users.get(id);
    }

    @Override
    public User update(final User user) {
        if (!_users.containsKey(user.getId())) {
            return null;
        }
        _users.put(user.getId(), user);
        _usersByUsername.put(user.getUsername(), user);
        return user;
    }

    @Override
    public boolean delete(final User user) {
        if (!_users.containsKey(user.getId())) {
            return false;
        }
        _users.remove(user.getId(), user);
        _usersByUsername.remove(user.getUsername(), user);
        return true;
    }

    private int getNextKey() {
        return _users.size() == 0 ? 0 : Collections.max(_users.keySet()) + 1;
    }
}
