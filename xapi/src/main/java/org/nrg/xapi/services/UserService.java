package org.nrg.xapi.services;

import org.nrg.xapi.model.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    List<String> getAllUsers();
    User getUserByUsername(final String username);
    List<User> getUsersByProperties(final Map<String, Object> properties);

    User create(final User user);
    User retrieve(final int id);
    User update(final User user);
    boolean delete(final User user);
}
