package org.nrg.xnat.poc.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import java.nio.file.Paths

class XnatTask extends DefaultTask {
    String greeting = 'Hello from XnatTask'

    @SuppressWarnings("GroovyUnusedDeclaration")
    @TaskAction
    def greet() {
        println "I have this greeting to give you: ${greeting}"
        println "Now I'm going to try to 'generate' some code."

        def folder = Paths.get(project.projectDir.absolutePath, "src/xnat/java/org/nrg/xnat/poc/dicom/generated").toFile()
        folder.mkdirs()
        Paths.get(folder.absolutePath, "GeneratedEntity.java").toFile().withWriter { out ->
            out.println code
        }
    }
    
    def code = """
package org.nrg.xnat.poc.dicom.generated;

import java.util.Date;

public class GeneratedEntity {
    public String getEntityMessage() {
        return String.format("I'm sending you this message at %tc", new Date());
    }
}
"""
}
