package org.nrg.xnat.poc.plugin

import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.Task
import org.gradle.api.tasks.compile.JavaCompile

class XnatPlugin implements Plugin<Project> {
    void apply(Project project) {
        def Task xnat = project.task('xnat', type: XnatTask)
        project.tasks.withType(JavaCompile) { def JavaCompile task ->
            task.dependsOn xnat
        }
        project.afterEvaluate {
            xnat.actions.each { action ->
                action.execute(xnat)
            }
        }
    }
}
